# xgboost approach to the Titanic problem
source('cleanup.R')

trainM <- data.matrix(train.proc[,c("Pclass", "Sex", "Age", "SibSp", "Parch", "Fare", 
						  "Embarked", "Title")], rownames.force = NA)
testM <- data.matrix(test.proc[,c("Pclass", "Sex", "Age", "SibSp", "Parch", "Fare", 
					   "Embarked", "Title")], rownames.force = NA)


train.y <- train.proc$Survived

dtrain <- xgb.DMatrix(data=trainM, label=train.y, missing = NaN)

watchlist <- list(trainM=dtrain)

set.seed(2017)


param <- list(  objective           = "binary:logistic", 
				booster             = "gbtree",
				eval_metric         = "error",
				eta                 = 0.3,
				max_depth           = 5,
				subsample           = 0.40,
				colsample_bytree    = 0.40
)

clf <- xgb.cv(  params              = param, 
				data                = dtrain, 
				nrounds             = 15000, 
				verbose             = 1,
				watchlist           = watchlist,
				maximize            = FALSE,
				nfold               = 5,
				early.stop.round    = 20,
				print.every.n       = 1
);

bestRound <- which.min( as.matrix(clf)[,3] );
cat("Best round:", bestRound,"\n");
cat("Best result:",min(as.matrix(clf)[,3]),"\n");

clf <- xgb.train(   params              = param, 
					data                = dtrain, 
					nrounds             = bestRound, 
					verbose             = 1,
					watchlist           = watchlist,
					maximize            = FALSE
)

testM <-data.matrix(test, rownames.force = NA)
preds <- predict(clf, testM,missing = NaN)


my_solution <- data.frame(PassengerId = test.proc$PassengerId, 
						  Survived = as.numeric(preds > max(preds)/2))

cat("The percentage of survivors is:",sum(my_solution$Survived)/nrow(my_solution))

write.csv(my_solution, "xgboost_solution.csv", row.names = FALSE)
